CREATE TABLE "place" (
  "id" uuid PRIMARY KEY,
  "name" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "board" (
  "id" uuid PRIMARY KEY,
  "place_id" uuid UNIQUE NOT NULL,
  "name" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "device" (
  "id" uuid PRIMARY KEY,
  "board_id" uuid NOT NULL,
  "name" varchar NOT NULL,
  "pin" integer NOT NULL,
  "state" boolean NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "device_association" (
  "id" uuid PRIMARY KEY,
  "first" uuid NOT NULL,
  "second" uuid NOT NULL,
  "state_change_delay" interval NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "api_key" (
  "id" uuid PRIMARY KEY,
  "user_id" uuid UNIQUE NOT NULL,
  "key" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "user" (
  "id" uuid PRIMARY KEY,
  "name" varchar NOT NULL,
  "login" varchar UNIQUE NOT NULL,
  "pass" varchar NOT NULL,
  "role" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

ALTER TABLE "api_key" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "device" ADD FOREIGN KEY ("board_id") REFERENCES "board" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "board" ADD FOREIGN KEY ("place_id") REFERENCES "place" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "device_association" ADD FOREIGN KEY ("first") REFERENCES "device" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "device_association" ADD FOREIGN KEY ("second") REFERENCES "device" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;