DELETE TABLE IF EXISTS user;
DELETE TABLE IF EXISTS api_key;
DELETE TABLE IF EXISTS device_association;
DELETE TABLE IF EXISTS device;
DELETE TABLE IF EXISTS board;
DELETE TABLE IF EXISTS place;
