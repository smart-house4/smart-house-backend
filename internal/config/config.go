package config

import (
	"fmt"

	"github.com/vrischmann/envconfig"
)

type Config struct {
	BASE struct {
		URL string
	}

	CERTS struct {
		Enable bool
	}

	App struct {
		Port        int
		ServiceName string
	}

	Pass struct {
		Salt string
	}

	Log struct {
		Level string
		Path  string
	}

	PG struct {
		User    string
		Pass    string
		Port    string
		Host    string
		PoolMax int
		DbName  string
		Timeout int
	}

	Token struct {
		Secret string
	}
}

func InitConfig(prefix string) (*Config, error) {
	conf := &Config{}
	if err := envconfig.InitWithPrefix(conf, prefix); err != nil {
		return nil, fmt.Errorf("init config error: %w", err)
	}

	return conf, nil
}
