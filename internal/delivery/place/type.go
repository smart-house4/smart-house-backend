package place

import "time"

type CreatePlaceRequest struct {
	Name string `json:"name" binding:"required"`
}

type UpdatePlaceRequest struct {
	Id   string `json:"id" binding:"required"`
	Name string `json:"name" binding:"required"`
}

type PlaceResponse struct {
	Id         string    `json:"id" binding:"required"`
	Name       string    `json:"name" binding:"required"`
	CreatedAt  time.Time `json:"createdAt"`
	ModifiedAt time.Time `json:"modifiedAt"`
}

type PlaceListResponse struct {
	List []*PlaceResponse `json:"list"`
}
