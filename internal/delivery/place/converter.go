package place

import "smart-house-backend/internal/domain/place"

func ToPlaceResponse(place *place.Place) *PlaceResponse {
	return &PlaceResponse{
		Id:         place.Id().String(),
		Name:       place.Name(),
		CreatedAt:  place.CreatedAt(),
		ModifiedAt: place.ModifiedAt(),
	}
}
