package delivery

import (
	"context"
	"net/http"
	jsonPlace "smart-house-backend/internal/delivery/place"
	"smart-house-backend/internal/domain/place"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreatePlace
// @Summary Создание места/комнаты.
// @Description Создание места/комнаты.
// @Tags place
// @Accept json
// @Produce json
// @Param data body jsonPlace.CreatePlaceRequest true "Body"
// @Success 201			{object} 	jsonPlace.PlaceResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /place/ [put]
func (d *Delivery) CreatePlace(c *gin.Context) {
	request := jsonPlace.CreatePlaceRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	place, err := place.NewPlace(request.Name)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Place.CreatePlace(context.Background(), place)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonPlace.ToPlaceResponse(place))
}

// UpdatePlace
// @Summary Обновление места/комнаты.
// @Description Обновление места/комнаты.
// @Tags place
// @Accept json
// @Produce json
// @Param data body jsonPlace.UpdatePlaceRequest true "Body"
// @Success 201			{object} 	jsonPlace.PlaceResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /place/ [post]
func (d *Delivery) UpdatePlace(c *gin.Context) {
	request := jsonPlace.UpdatePlaceRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.Id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	newPlace, err := place.NewPlaceWithId(id, request.Name, time.Now(), time.Now())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	upFn := func(oldPlace *place.Place) (*place.Place, error) {
		return place.NewPlaceWithId(oldPlace.Id(), newPlace.Name(), oldPlace.CreatedAt(), newPlace.ModifiedAt())
	}

	resultPlace, err := d.services.Place.UpdatePlace(context.Background(), newPlace.Id(), upFn)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonPlace.ToPlaceResponse(resultPlace))
}

// DeletePlace
// @Summary Удаление места/комнаты.
// @Description Удаление места/комнаты.
// @Tags place
// @Produce json
// @Param id path string true "Place ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /place/{id} [delete]
func (d *Delivery) DeletePlace(c *gin.Context) {
	id := c.Param("id")
	placeId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Place.DeletePlace(context.Background(), placeId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadPlaceList
// @Summary Список мест/комнат.
// @Description Список мест/комнат.
// @Tags place
// @Produce json
// @Success 200			{object} 	jsonPlace.PlaceListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /place/ [get]
func (d *Delivery) ReadPlaceList(c *gin.Context) {
	placeList, err := d.services.Place.ReadPlacesList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	placeListResponse := make([]*jsonPlace.PlaceResponse, len(placeList))

	for index, place := range placeList {
		placeListResponse[index] = jsonPlace.ToPlaceResponse(place)
	}

	c.JSON(http.StatusOK, jsonPlace.PlaceListResponse{List: placeListResponse})
}
