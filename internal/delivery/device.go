package delivery

import (
	"context"
	"net/http"
	jsonDevice "smart-house-backend/internal/delivery/device"
	"smart-house-backend/internal/domain/device"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateDevice
// @Summary Создание устройства.
// @Description Создание устройства.
// @Tags device
// @Accept json
// @Produce json
// @Param data body jsonDevice.CreateDeviceRequest true "Body"
// @Success 201			{object} 	jsonDevice.DeviceResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /device/ [put]
func (d *Delivery) CreateDevice(c *gin.Context) {
	request := jsonDevice.CreateDeviceRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	boardId, err := uuid.Parse(request.BoardId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	device, err := device.NewDevice(boardId, request.Pin, request.Name, request.State)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Device.CreateDevice(context.Background(), device)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonDevice.ToDeviceResponse(device))
}

// UpdateDevice
// @Summary Обновление устройства.
// @Description Обновление устройства.
// @Tags device
// @Accept json
// @Produce json
// @Param data body jsonDevice.UpdateDeviceRequest true "Body"
// @Success 201			{object} 	jsonDevice.DeviceResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /device/ [post]
func (d *Delivery) UpdateDevice(c *gin.Context) {
	request := jsonDevice.UpdateDeviceRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.Id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	boardId, err := uuid.Parse(request.BoardId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	newDevice, err := device.NewDeviceWithId(id, boardId, request.Name, request.Pin, request.State, time.Now(), time.Now())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	upFn := func(oldDevice *device.Device) (*device.Device, error) {
		return device.NewDeviceWithId(oldDevice.Id(), newDevice.BoardId(), newDevice.Name(), newDevice.Pin(), newDevice.State(), oldDevice.CreatedAt(), newDevice.ModifiedAt())
	}

	resultDevice, err := d.services.Device.UpdateDevice(context.Background(), newDevice.Id(), upFn)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonDevice.ToDeviceResponse(resultDevice))
}

// DeleteDevice
// @Summary Удаление устройства.
// @Description Удаление устройства.
// @Tags device
// @Produce json
// @Param id path string true "Device ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /device/{id} [delete]
func (d *Delivery) DeleteDevice(c *gin.Context) {
	id := c.Param("id")
	deviceId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Device.DeleteDevice(context.Background(), deviceId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadDeviceList
// @Summary Список устройств.
// @Description Список устройств.
// @Tags device
// @Produce json
// @Success 200			{object} 	jsonDevice.DeviceListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /device/ [get]
func (d *Delivery) ReadDeviceList(c *gin.Context) {
	deviceList, err := d.services.Device.ReadDevicesList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	deviceListResponse := make([]*jsonDevice.DeviceResponse, len(deviceList))

	for index, device := range deviceList {
		deviceListResponse[index] = jsonDevice.ToDeviceResponse(device)
	}

	c.JSON(http.StatusOK, jsonDevice.DeviceListResponse{List: deviceListResponse})
}

// UpdateDeviceState
// @Summary Обновление состояния устройства (нужно передавать api key).
// @Description Обновление состояния устройства (нужно передавать api key).
// @Tags device
// @Accept json
// @Produce json
// @Param key path string true "ApiKey"
// @Param id path string true "Device ID"
// @Param state path string true "New State"
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /device/{key}/{id}/{state} [get]
func (d *Delivery) UpdateDeviceState(c *gin.Context) {
	key := c.Param("key")

	stringState := c.Param("state")
	state := false

	switch stringState {
	case "true":
		state = true
	case "false":
		state = false
	default:
		SetError(c, http.StatusBadRequest, "not valid state provided")
		return
	}

	id := c.Param("id")
	deviceId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	go func() {
		_, err = d.services.Device.UpdateDeviceState(context.Background(), deviceId, key, state)
		if err != nil {
			d.logger.Error("error updating device state: %s", err.Error())
			return
		}
	}()

	c.Status(http.StatusOK)
}
