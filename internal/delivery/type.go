package delivery

import "github.com/google/uuid"

type AuthParams struct {
	UserId   uuid.UUID
	UserName string
	UserRole string
}
