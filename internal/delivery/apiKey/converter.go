package apiKey

import "smart-house-backend/internal/domain/apiKey"

func ToApiKeyResponse(apiKey *apiKey.ApiKey) *ApiKeyResponse {
	return &ApiKeyResponse{
		ID:         apiKey.Id().String(),
		UserId:     apiKey.UserId().String(),
		Key:        apiKey.Key(),
		CreatedAt:  apiKey.CreatedAt(),
		ModifiedAt: apiKey.ModifiedAt(),
	}
}
