package apiKey

import "time"

type CreateApiKeyRequest struct {
	UserId string `json:"userId" binding:"required"`
}

type ApiKeyResponse struct {
	ID         string    `json:"id"`
	UserId     string    `json:"userId"`
	Key        string    `json:"key"`
	CreatedAt  time.Time `json:"createdAt"`
	ModifiedAt time.Time `json:"modifiedAt"`
}

type ApiKeyListResponse struct {
	List []*ApiKeyResponse `json:"list"`
}
