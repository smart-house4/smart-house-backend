package delivery

import (
	docs "smart-house-backend/internal/delivery/swagger/docs"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title smart-house backend service
// @version 1.0
// @description smart-house backend service
// @license.name kanya384

// @contact.name API Support
// @contact.email kanya384@mail.ru

// @BasePath /
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func (d *Delivery) initRouter() *gin.Engine {

	var router = gin.New()
	router.Use(cors.Default())

	router.Static("/file-store", "./file-store")
	router.GET("/board/:key/:id", d.ReadBoardPinStates)
	router.GET("/device/:key/:id/:state", d.UpdateDeviceState)

	d.routerDocs(router.Group("/docs"))
	d.routerAuth(router.Group("/auth"))

	router.Use(d.checkAuth)

	d.routerApiKey(router.Group("/apiKey"))
	d.routerPlace(router.Group("/place"))
	d.routerDevice(router.Group("/device"))
	d.routerBoard(router.Group("/board"))

	return router
}

func (d *Delivery) routerAuth(router *gin.RouterGroup) {
	router.POST("/sign-in", d.SignIn)
}

func (d *Delivery) routerApiKey(router *gin.RouterGroup) {
	router.GET("/", d.ReadApiKeyList)
	router.PUT("/", d.CreateApiKey)
	router.DELETE("/:id", d.DeleteApiKey)
}

func (d *Delivery) routerPlace(router *gin.RouterGroup) {
	router.GET("/", d.ReadPlaceList)
	router.PUT("/", d.CreatePlace)
	router.POST("/", d.UpdatePlace)
	router.DELETE("/:id", d.DeletePlace)
}

func (d *Delivery) routerDevice(router *gin.RouterGroup) {
	router.GET("/", d.ReadDeviceList)
	router.PUT("/", d.CreateDevice)
	router.POST("/", d.UpdateDevice)
	router.DELETE("/:id", d.DeleteDevice)

	router.GET("/association", d.ReadDeviceAssociationList)
	router.PUT("/association", d.CreateDeviceAssociation)
	router.POST("/association", d.UpdateDeviceAssociation)
	router.DELETE("/association/:id", d.DeleteDeviceAssociation)
}

func (d *Delivery) routerBoard(router *gin.RouterGroup) {
	router.GET("/", d.ReadBoardList)
	router.PUT("/", d.CreateBoard)
	router.POST("/", d.UpdateBoard)
	router.DELETE("/:id", d.DeleteBoard)
}

func (d *Delivery) routerDocs(router *gin.RouterGroup) {
	docs.SwaggerInfo.BasePath = "/"

	router.Any("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
