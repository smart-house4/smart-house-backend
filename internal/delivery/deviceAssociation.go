package delivery

import (
	"context"
	"net/http"
	jsonDeviceAssociation "smart-house-backend/internal/delivery/deviceAssociation"
	"smart-house-backend/internal/domain/device"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateDeviceAssociation
// @Summary Создание связки устройств.
// @Description Создание связки устройств.
// @Tags device
// @Accept json
// @Produce json
// @Param data body jsonDeviceAssociation.CreateDeviceAssociationRequest true "Body"
// @Success 201			{object} 	jsonDeviceAssociation.DeviceAssociationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /device/association [put]
func (d *Delivery) CreateDeviceAssociation(c *gin.Context) {
	request := jsonDeviceAssociation.CreateDeviceAssociationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	first, err := uuid.Parse(request.First)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	second, err := uuid.Parse(request.Second)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	device, err := device.NewDeviceAssociation(first, second, request.StateChangeDelay)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Device.CreateDeviceAssociation(context.Background(), device)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonDeviceAssociation.ToDeviceAssociationResponse(device))
}

// UpdateDeviceAssociation
// @Summary Обновление связки устройств.
// @Description Обновление связки  устройств.
// @Tags device
// @Accept json
// @Produce json
// @Param data body jsonDeviceAssociation.UpdateDeviceAssociationRequest true "Body"
// @Success 201			{object} 	jsonDeviceAssociation.DeviceAssociationResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /device/association [post]
func (d *Delivery) UpdateDeviceAssociation(c *gin.Context) {
	request := jsonDeviceAssociation.UpdateDeviceAssociationRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.Id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	first, err := uuid.Parse(request.First)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	second, err := uuid.Parse(request.Second)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	upFn := func(oldDeviceAssociation *device.DeviceAssociation) (*device.DeviceAssociation, error) {
		return device.NewDeviceAssociationWithId(oldDeviceAssociation.Id(), first, second, request.StateChangeDelay, oldDeviceAssociation.CreatedAt(), time.Now())
	}

	resultDeviceAssociation, err := d.services.Device.UpdateDeviceAssociation(context.Background(), id, upFn)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonDeviceAssociation.ToDeviceAssociationResponse(resultDeviceAssociation))
}

// DeleteDeviceAssociation
// @Summary Удаление связки устройств.
// @Description Удаление связки устройств.
// @Tags device
// @Produce json
// @Param id path string true "DeviceAssociation ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /device/association/{id} [delete]
func (d *Delivery) DeleteDeviceAssociation(c *gin.Context) {
	id := c.Param("id")
	deviceId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Device.DeleteDeviceAssociation(context.Background(), deviceId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadDeviceAssociationList
// @Summary Список связки устройств.
// @Description Список связки устройств.
// @Tags device
// @Produce json
// @Success 200			{object} 	jsonDeviceAssociation.DeviceAssociationListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /device/association [get]
func (d *Delivery) ReadDeviceAssociationList(c *gin.Context) {
	deviceList, err := d.services.Device.ReadDeviceAssociationsList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	deviceListResponse := make([]*jsonDeviceAssociation.DeviceAssociationResponse, len(deviceList))

	for index, device := range deviceList {
		deviceListResponse[index] = jsonDeviceAssociation.ToDeviceAssociationResponse(device)
	}

	c.JSON(http.StatusOK, jsonDeviceAssociation.DeviceAssociationListResponse{List: deviceListResponse})
}
