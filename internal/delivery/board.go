package delivery

import (
	"context"
	"net/http"
	jsonBoard "smart-house-backend/internal/delivery/board"
	"smart-house-backend/internal/domain/board"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateBoard
// @Summary Создание платы.
// @Description Создание платы.
// @Tags board
// @Accept json
// @Produce json
// @Param data body jsonBoard.CreateBoardRequest true "Body"
// @Success 201			{object} 	jsonBoard.BoardResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /board/ [put]
func (d *Delivery) CreateBoard(c *gin.Context) {
	request := jsonBoard.CreateBoardRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	placeId, err := uuid.Parse(request.PlaceId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	board, err := board.NewBoard(placeId, request.Name)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Board.CreateBoard(context.Background(), board)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonBoard.ToBoardResponse(board))
}

// UpdateBoard
// @Summary Обновление платы.
// @Description Обновление платы.
// @Tags board
// @Accept json
// @Produce json
// @Param data body jsonBoard.UpdateBoardRequest true "Body"
// @Success 201			{object} 	jsonBoard.BoardResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /board/ [post]
func (d *Delivery) UpdateBoard(c *gin.Context) {
	request := jsonBoard.UpdateBoardRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := uuid.Parse(request.Id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	placeId, err := uuid.Parse(request.PlaceId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	newBoard, err := board.NewBoardWithId(id, placeId, request.Name, time.Now(), time.Now())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	upFn := func(oldBoard *board.Board) (*board.Board, error) {
		return board.NewBoardWithId(oldBoard.Id(), newBoard.PlaceId(), newBoard.Name(), oldBoard.CreatedAt(), newBoard.ModifiedAt())
	}

	resultBoard, err := d.services.Board.UpdateBoard(context.Background(), newBoard.Id(), upFn)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonBoard.ToBoardResponse(resultBoard))
}

// DeleteBoard
// @Summary Удаление платы.
// @Description Удаление платы.
// @Tags board
// @Produce json
// @Param id path string true "Board ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /board/{id} [delete]
func (d *Delivery) DeleteBoard(c *gin.Context) {
	id := c.Param("id")
	boardId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.Board.DeleteBoard(context.Background(), boardId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadBoardList
// @Summary Список плат.
// @Description Список плат.
// @Tags board
// @Produce json
// @Success 200			{object} 	jsonBoard.BoardListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /board/ [get]
func (d *Delivery) ReadBoardList(c *gin.Context) {
	boardList, err := d.services.Board.ReadBoardsList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	boardListResponse := make([]*jsonBoard.BoardResponse, len(boardList))

	for index, board := range boardList {
		boardListResponse[index] = jsonBoard.ToBoardResponse(board)
	}

	c.JSON(http.StatusOK, jsonBoard.BoardListResponse{List: boardListResponse})
}

// ReadBoardPinStates
// @Summary Возвращает состояние пинов устройства (нужно передавать api key).
// @Description Возвращает состояние пинов устройства (нужно передавать api key).
// @Tags board
// @Accept json
// @Produce json
// @Param key path string true "ApiKey"
// @Param id path string true "Device ID"
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /board/{key}/{id} [get]
func (d *Delivery) ReadBoardPinStates(c *gin.Context) {
	key := c.Param("key")

	id := c.Param("id")
	deviceId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	pinStates, err := d.services.Board.ReadBoardPinStates(context.Background(), key, deviceId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonBoard.ToBoardResponsePinStates(pinStates))
}
