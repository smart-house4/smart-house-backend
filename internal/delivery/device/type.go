package device

import "time"

type CreateDeviceRequest struct {
	BoardId string `json:"boardId" binding:"required"`
	Name    string `json:"name" binding:"required"`
	Pin     int    `json:"pin" binding:"required"`
	State   bool   `json:"state"`
}

type UpdateDeviceRequest struct {
	Id      string `json:"id" binding:"required"`
	BoardId string `json:"boardId" binding:"required"`
	Name    string `json:"name" binding:"required"`
	Pin     int    `json:"pin" binding:"required"`
	State   bool   `json:"state" binding:"required"`
}

type DeviceResponse struct {
	Id         string    `json:"id"`
	BoardId    string    `json:"boardId"`
	Name       string    `json:"name"`
	Pin        int       `json:"pin"`
	State      bool      `json:"state"`
	CreatedAt  time.Time `json:"createdAt"`
	ModifiedAt time.Time `json:"modifiedAt"`
}

type DeviceListResponse struct {
	List []*DeviceResponse `json:"list"`
}
