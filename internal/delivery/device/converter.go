package device

import "smart-house-backend/internal/domain/device"

func ToDeviceResponse(device *device.Device) *DeviceResponse {
	return &DeviceResponse{
		Id:         device.Id().String(),
		BoardId:    device.BoardId().String(),
		Name:       device.Name(),
		Pin:        device.Pin(),
		State:      device.State(),
		CreatedAt:  device.CreatedAt(),
		ModifiedAt: device.ModifiedAt(),
	}
}
