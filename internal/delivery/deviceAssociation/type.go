package deviceAssociation

import (
	"time"
)

type CreateDeviceAssociationRequest struct {
	First            string        `json:"first" binding:"required"`
	Second           string        `json:"second" binding:"required"`
	StateChangeDelay time.Duration `json:"stateChangeDelay" binding:"required"`
}

type UpdateDeviceAssociationRequest struct {
	Id               string        `json:"id" binding:"required"`
	First            string        `json:"first" binding:"required"`
	Second           string        `json:"second" binding:"required"`
	StateChangeDelay time.Duration `json:"stateChangeDelay" binding:"required"`
}

type DeviceAssociationResponse struct {
	Id               string        `json:"id" binding:"required"`
	First            string        `json:"first" binding:"required"`
	Second           string        `json:"second" binding:"required"`
	StateChangeDelay time.Duration `json:"stateChangeDelay" binding:"required"`
	CreatedAt        time.Time     `json:"createdAt"`
	ModifiedAt       time.Time     `json:"modifiedAt"`
}

type DeviceAssociationListResponse struct {
	List []*DeviceAssociationResponse `json:"list"`
}
