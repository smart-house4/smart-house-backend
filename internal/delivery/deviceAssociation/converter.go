package deviceAssociation

import "smart-house-backend/internal/domain/device"

func ToDeviceAssociationResponse(device *device.DeviceAssociation) *DeviceAssociationResponse {
	return &DeviceAssociationResponse{
		Id:               device.Id().String(),
		First:            device.First().String(),
		Second:           device.Second().String(),
		StateChangeDelay: device.StateChangeDelay(),
		CreatedAt:        device.CreatedAt(),
		ModifiedAt:       device.ModifiedAt(),
	}
}
