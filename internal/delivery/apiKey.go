package delivery

import (
	"context"
	"net/http"
	jsonApiKey "smart-house-backend/internal/delivery/apiKey"
	"smart-house-backend/internal/domain/apiKey"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CrateApiKey
// @Summary Создание api ключа.
// @Description Создание api ключа.
// @Tags apiKey
// @Accept json
// @Produce json
// @Param data body jsonApiKey.CreateApiKeyRequest true "Body"
// @Success 201			{object} 	jsonApiKey.ApiKeyResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /apiKey/ [put]
func (d *Delivery) CreateApiKey(c *gin.Context) {
	request := jsonApiKey.CreateApiKeyRequest{}
	if err := c.ShouldBind(&request); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	userId, err := uuid.Parse(request.UserId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	apiKey, err := apiKey.NewApiKey(userId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.ApiKey.CreateApiKey(context.Background(), apiKey)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, jsonApiKey.ToApiKeyResponse(apiKey))
}

// DeleteApiKey
// @Summary Удаление api ключа.
// @Description Удаление api ключа.
// @Tags apiKey
// @Accept json
// @Produce json
// @Param id path string true "ApiKey ID"
// @Produce json
// @Success 200
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /apiKey/{id} [delete]
func (d *Delivery) DeleteApiKey(c *gin.Context) {
	id := c.Param("id")
	apiKeyId, err := uuid.Parse(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.ApiKey.DeleteApiKey(context.Background(), apiKeyId)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusOK)
}

// ReadApiKeyList
// @Summary Список ключей.
// @Description Список ключей.
// @Tags apiKey
// @Produce json
// @Success 200			{object} 	jsonApiKey.ApiKeyListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /apiKey/ [get]
func (d *Delivery) ReadApiKeyList(c *gin.Context) {
	apiKeyList, err := d.services.ApiKey.ReadApiKeyList(context.Background())
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	apiKeyListResponse := make([]*jsonApiKey.ApiKeyResponse, len(apiKeyList))

	for index, apiKey := range apiKeyList {
		apiKeyListResponse[index] = jsonApiKey.ToApiKeyResponse(apiKey)
	}

	c.JSON(http.StatusOK, jsonApiKey.ApiKeyListResponse{List: apiKeyListResponse})
}
