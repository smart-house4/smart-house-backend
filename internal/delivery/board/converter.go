package board

import (
	"smart-house-backend/internal/domain/board"
	serviceBoard "smart-house-backend/internal/service/board"
)

func ToBoardResponse(board *board.Board) *BoardResponse {
	return &BoardResponse{
		Id:         board.Id().String(),
		PlaceId:    board.PlaceId().String(),
		Name:       board.Name(),
		CreatedAt:  board.CreatedAt(),
		ModifiedAt: board.ModifiedAt(),
	}
}

func ToBoardResponsePinStates(boardStates []*serviceBoard.PinState) *ReadBoardPinStatesResponse {
	states := make([]*PinStateResponse, len(boardStates))
	for index, state := range boardStates {
		states[index] = &PinStateResponse{
			Pin:   state.Pin,
			State: state.State,
		}
	}
	return &ReadBoardPinStatesResponse{
		States: states,
	}
}
