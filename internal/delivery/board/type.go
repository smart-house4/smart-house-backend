package board

import "time"

type CreateBoardRequest struct {
	PlaceId string `json:"placeId" binding:"required"`
	Name    string `json:"name" binding:"required"`
}

type UpdateBoardRequest struct {
	Id      string `json:"id" binding:"required"`
	PlaceId string `json:"placeId" binding:"required"`
	Name    string `json:"name" binding:"required"`
}

type BoardResponse struct {
	Id         string    `json:"id" binding:"required"`
	PlaceId    string    `json:"placeId" binding:"required"`
	Name       string    `json:"name" binding:"required"`
	CreatedAt  time.Time `json:"createdAt"`
	ModifiedAt time.Time `json:"modifiedAt"`
}

type BoardListResponse struct {
	List []*BoardResponse `json:"list"`
}

type ReadBoardPinStatesResponse struct {
	States []*PinStateResponse `json:"states"`
}

type PinStateResponse struct {
	Pin   int  `json:"pin"`
	State bool `json:"state"`
}
