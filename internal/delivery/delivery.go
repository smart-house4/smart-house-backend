package delivery

import (
	"fmt"
	"net/http"
	"smart-house-backend/internal/service"
	"strings"

	"github.com/gin-gonic/autotls"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/kanya384/gotools/auth"
	"gitlab.com/kanya384/gotools/logger"
	"golang.org/x/crypto/acme/autocert"
)

type Delivery struct {
	services    *service.Service
	router      *gin.Engine
	tm          auth.TokenManager
	logger      logger.Interface
	enableCerts bool
	baseUrl     string

	options Options
}

type Options struct{}

func New(services *service.Service, signingKey string, baseUrl string, enableCerts bool, logger logger.Interface, options Options) (*Delivery, error) {
	tm, err := auth.NewManager(signingKey)
	if err != nil {
		return nil, err
	}
	var d = &Delivery{
		services:    services,
		baseUrl:     baseUrl,
		enableCerts: enableCerts,
		logger:      logger,
		tm:          tm,
	}

	d.SetOptions(options)

	d.router = d.initRouter()
	return d, nil
}

func (d *Delivery) SetOptions(options Options) {
	if d.options != options {
		d.options = options
	}
}

func (d *Delivery) Run(port int) error {
	if !d.enableCerts {
		return d.router.Run(fmt.Sprintf(":%d", port))
	}
	m := autocert.Manager{
		Prompt:     autocert.AcceptTOS,
		HostPolicy: autocert.HostWhitelist(d.baseUrl),
		Cache:      autocert.DirCache("./certs"),
	}

	return autotls.RunWithManager(d.router, &m)
}

func (d *Delivery) checkAuth(c *gin.Context) {
	token := c.GetHeader("Authorization")
	if token == "" || strings.Contains("Bearer ", token) {
		c.AbortWithStatusJSON(http.StatusUnauthorized, "please authorize")
		return
	}

	token = strings.Replace(token, "Bearer ", "", 1)

	result, err := d.tm.Parse(token)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, "please authorize")
		return
	}
	id, _ := uuid.Parse(result.UserID)

	setAuthInfoToContext(c, &AuthParams{
		UserId:   id,
		UserName: result.UserName,
		UserRole: result.UserRole,
	})

	c.Next()
}

func setAuthInfoToContext(c *gin.Context, params *AuthParams) {
	c.Set("userId", params.UserId)
	c.Set("userName", params.UserName)
	c.Set("userRole", params.UserRole)
}

/*func getAuthInfoFromContext(c *gin.Context) (params *AuthParams, err error) {
	userId, ok := c.Get("userId")
	if !ok {
		return nil, errors.New("not valid auth")
	}

	userName, ok := c.Get("userName")
	if !ok {
		return nil, errors.New("not valid auth")
	}

	userRole, ok := c.Get("userRole")
	if !ok {
		return nil, errors.New("not valid auth")
	}

	return &AuthParams{
		UserId:   userId.(uuid.UUID),
		UserName: userName.(string),
		UserRole: userRole.(string),
	}, nil
}*/
