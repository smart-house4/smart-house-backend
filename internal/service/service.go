package service

import (
	"smart-house-backend/internal/repository"
	"smart-house-backend/internal/service/apiKey"
	"smart-house-backend/internal/service/board"
	"smart-house-backend/internal/service/device"
	"smart-house-backend/internal/service/place"
	"smart-house-backend/internal/service/user"
	"time"

	"gitlab.com/kanya384/gotools/logger"
)

const (
	tokenTTLDefault = time.Hour * 2
)

type Service struct {
	ApiKey apiKey.ApiKey
	Board  board.Board
	Device device.Device
	Place  place.Place
	Auth   user.Service
}

func NewServices(repository *repository.Repository, logger logger.Interface, tokenSecret string, passSalt string) (*Service, error) {
	apiKey, err := apiKey.New(repository, logger, apiKey.Options{})
	if err != nil {
		return nil, err
	}

	board, err := board.New(repository, logger, board.Options{})
	if err != nil {
		return nil, err
	}

	device, err := device.New(repository, logger, device.Options{})
	if err != nil {
		return nil, err
	}

	place, err := place.New(repository, logger, place.Options{})
	if err != nil {
		return nil, err
	}

	user, err := user.New(repository, tokenSecret, passSalt, logger, user.Options{
		TokenTTL: tokenTTLDefault,
	})
	if err != nil {
		return nil, err
	}

	return &Service{
		apiKey,
		board,
		device,
		place,
		user,
	}, nil
}
