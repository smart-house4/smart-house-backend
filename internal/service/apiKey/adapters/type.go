package repository

import (
	"context"
	"smart-house-backend/internal/domain/apiKey"

	"github.com/google/uuid"
)

type Repository interface {
	CreateApiKey(ctx context.Context, apiKey *apiKey.ApiKey) (err error)
	UpdateApiKey(ctx context.Context, id uuid.UUID, updateFn func(apiKey *apiKey.ApiKey) (*apiKey.ApiKey, error)) (apiKey *apiKey.ApiKey, err error)
	DeleteApiKey(ctx context.Context, id uuid.UUID) (err error)
	FindApiKeyByKey(ctx context.Context, key string) (apiKey *apiKey.ApiKey, err error)
	ReadApiKeyList(ctx context.Context) (statuses []*apiKey.ApiKey, err error)
}
