package apiKey

import (
	"context"
	"smart-house-backend/internal/domain/apiKey"

	"github.com/google/uuid"
)

func (s *service) CreateApiKey(ctx context.Context, apiKey *apiKey.ApiKey) (err error) {
	return s.repository.ApiKey.CreateApiKey(ctx, apiKey)
}

func (s *service) UpdateApiKey(ctx context.Context, id uuid.UUID, updateFn func(apiKey *apiKey.ApiKey) (*apiKey.ApiKey, error)) (apiKey *apiKey.ApiKey, err error) {
	return s.repository.ApiKey.UpdateApiKey(ctx, id, updateFn)
}

func (s *service) DeleteApiKey(ctx context.Context, id uuid.UUID) (err error) {
	return s.repository.ApiKey.DeleteApiKey(ctx, id)
}

func (s *service) FindApiKeyByKey(ctx context.Context, key string) (apiKey *apiKey.ApiKey, err error) {
	return s.repository.ApiKey.FindApiKeyByKey(ctx, key)
}

func (s *service) ReadApiKeyList(ctx context.Context) (list []*apiKey.ApiKey, err error) {
	return s.repository.ApiKey.ReadApiKeyList(ctx)
}
