package place

import (
	"context"
	"smart-house-backend/internal/domain/place"

	"github.com/google/uuid"
)

type Place interface {
	CreatePlace(ctx context.Context, place *place.Place) (err error)
	UpdatePlace(ctx context.Context, id uuid.UUID, updateFn func(place *place.Place) (*place.Place, error)) (place *place.Place, err error)
	DeletePlace(ctx context.Context, id uuid.UUID) (err error)
	FindPlaceById(ctx context.Context, id uuid.UUID) (place *place.Place, err error)
	ReadPlacesList(ctx context.Context) (places []*place.Place, err error)
}
