package place

import (
	"context"
	"smart-house-backend/internal/domain/place"

	"github.com/google/uuid"
)

func (s *service) CreatePlace(ctx context.Context, place *place.Place) (err error) {
	return s.repository.Place.CreatePlace(ctx, place)
}

func (s *service) UpdatePlace(ctx context.Context, id uuid.UUID, updateFn func(place *place.Place) (*place.Place, error)) (place *place.Place, err error) {
	return s.repository.Place.UpdatePlace(ctx, id, updateFn)
}

func (s *service) DeletePlace(ctx context.Context, id uuid.UUID) (err error) {
	return s.repository.Place.DeletePlace(ctx, id)
}

func (s *service) FindPlaceById(ctx context.Context, id uuid.UUID) (place *place.Place, err error) {
	return s.repository.Place.FindPlaceById(ctx, id)
}

func (s *service) ReadPlacesList(ctx context.Context) (places []*place.Place, err error) {
	return s.repository.Place.ReadPlacesList(ctx)
}
