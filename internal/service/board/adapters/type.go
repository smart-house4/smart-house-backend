package repository

import (
	"context"
	"smart-house-backend/internal/domain/board"

	"github.com/google/uuid"
)

type Repository interface {
	CreateBoard(ctx context.Context, board *board.Board) (err error)
	UpdateBoard(ctx context.Context, id uuid.UUID, updateFn func(board *board.Board) (*board.Board, error)) (board *board.Board, err error)
	DeleteBoard(ctx context.Context, id uuid.UUID) (err error)
	FindBoardById(ctx context.Context, id uuid.UUID) (board *board.Board, err error)
	ReadBoardsList(ctx context.Context) (statuses []*board.Board, err error)
}
