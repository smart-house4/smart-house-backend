package board

import (
	"context"
	"fmt"
	"smart-house-backend/internal/domain/board"

	"github.com/google/uuid"
)

func (s *service) CreateBoard(ctx context.Context, board *board.Board) (err error) {
	return s.repository.Board.CreateBoard(ctx, board)
}

func (s *service) UpdateBoard(ctx context.Context, id uuid.UUID, updateFn func(board *board.Board) (*board.Board, error)) (board *board.Board, err error) {
	return s.repository.Board.UpdateBoard(ctx, id, updateFn)
}

func (s *service) DeleteBoard(ctx context.Context, id uuid.UUID) (err error) {
	return s.repository.Board.DeleteBoard(ctx, id)
}

func (s *service) FindBoardById(ctx context.Context, id uuid.UUID) (board *board.Board, err error) {
	return s.repository.Board.FindBoardById(ctx, id)
}

func (s *service) ReadBoardPinStates(ctx context.Context, apiKey string, id uuid.UUID) (states []*PinState, err error) {
	_, err = s.repository.ApiKey.FindApiKeyByKey(ctx, apiKey)
	if err != nil {
		return nil, fmt.Errorf("api key not exists: %s", err.Error())
	}

	devices, err := s.repository.Device.FindDevicesByBoardId(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("api key not exists: %s", err.Error())
	}

	states = make([]*PinState, len(devices))

	for index, device := range devices {
		states[index] = &PinState{
			Pin:   device.Pin(),
			State: device.State(),
		}
	}
	return
}

func (s *service) ReadBoardsList(ctx context.Context) (statuses []*board.Board, err error) {
	return s.repository.Board.ReadBoardsList(ctx)
}
