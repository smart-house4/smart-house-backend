package user

import (
	"smart-house-backend/internal/repository"
	"time"

	"gitlab.com/kanya384/gotools/auth"
	"gitlab.com/kanya384/gotools/logger"
)

type service struct {
	repository   *repository.Repository
	logger       logger.Interface
	tokenManager auth.TokenManager
	passSalt     string
	options      Options
}

type Options struct {
	TokenTTL time.Duration
}

func (uc *service) SetOptions(options Options) {
	if uc.options != options {
		uc.options = options
	}
}

func New(repository *repository.Repository, tokenSecret, passSalt string, logger logger.Interface, options Options) (*service, error) {
	tokenManager, err := auth.NewManager(tokenSecret)
	if err != nil {
		return nil, err
	}
	service := &service{
		repository:   repository,
		logger:       logger,
		tokenManager: tokenManager,
		passSalt:     passSalt,
	}

	service.SetOptions(options)
	return service, err
}
