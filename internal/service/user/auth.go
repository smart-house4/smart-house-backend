package user

import (
	"context"
	"crypto/sha256"
	"fmt"
	"smart-house-backend/internal/domain/user"

	"github.com/google/uuid"
	"gitlab.com/kanya384/gotools/auth"
)

func (s *service) SignIn(ctx context.Context, login string, pass string) (res SignInResult, err error) {
	pass = generateHashPassword(pass, s.passSalt)
	user, err := s.repository.User.FindUserByCredetinals(ctx, login, pass)
	if err != nil {
		s.logger.Info("SignIn user not found: %s", err.Error())
		return
	}

	token, err := s.tokenManager.NewJWT(auth.JwtClaims{UserID: user.Id().String(), UserName: user.Name()}, s.options.TokenTTL)
	if err != nil {
		s.logger.Error("SignIn token generate error: %s", err.Error())
		return
	}

	res.Token = token
	//TODO: implement refresh token generation and save
	return
}

func (s *service) SignUp(ctx context.Context, user *user.User) (err error) {
	user.SetPass(generateHashPassword(user.Pass(), s.passSalt))
	err = s.repository.User.CreateUser(ctx, user)
	if err != nil {
		s.logger.Error("SignUp error: %s", err.Error())
		return
	}
	return
}

func (s *service) RefreshToken(ctx context.Context, userID uuid.UUID, refreshToken string) (result RefreshResult, err error) {
	panic("not implemented") // TODO: Implement
}

func generateHashPassword(password string, salt string) string {
	hash := sha256.New()
	hash.Write([]byte(password))
	return fmt.Sprintf("%x", hash.Sum([]byte(salt)))
}
