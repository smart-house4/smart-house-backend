package device

import (
	"context"
	"fmt"
	"smart-house-backend/internal/domain/device"
	"time"

	"github.com/google/uuid"
)

const (
	defaultTurnOfTimeForAssociations = time.Second * 30
)

func (s *service) CreateDevice(ctx context.Context, device *device.Device) (err error) {
	return s.repository.Device.CreateDevice(ctx, device)
}

func (s *service) UpdateDevice(ctx context.Context, id uuid.UUID, updateFn func(device *device.Device) (*device.Device, error)) (device *device.Device, err error) {
	return s.repository.Device.UpdateDevice(ctx, id, updateFn)
}

func (s *service) UpdateDeviceState(ctx context.Context, id uuid.UUID, apiKey string, newState bool) (result *device.Device, err error) {
	_, err = s.repository.ApiKey.FindApiKeyByKey(ctx, apiKey)
	if err != nil {
		return nil, fmt.Errorf("api key not exists: %s", err.Error())
	}

	oldDevice, err := s.repository.Device.FindDeviceById(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("device not exists: %s", err.Error())
	}

	if oldDevice.State() == newState {
		return nil, fmt.Errorf("nothing to change")
	}

	association, err := s.repository.DeviceAssociation.FindDeviceAssociationByDeviceId(ctx, id)
	if err == nil && association != nil {
		// if association exists, then switch of second device
		secondDeviceId := association.First()
		if secondDeviceId == id {
			secondDeviceId = association.Second()
		}

		secondDevice, err := s.repository.Device.FindDeviceById(ctx, secondDeviceId)
		if err != nil {
			return nil, fmt.Errorf("second device not founded by id: %s", err.Error())
		}

		if secondDevice.State() {
			upSecondDeviceFn := func(oldDevice *device.Device) (*device.Device, error) {
				return device.NewDeviceWithId(oldDevice.Id(), oldDevice.BoardId(), oldDevice.Name(), oldDevice.Pin(), false, oldDevice.CreatedAt(), time.Now())
			}

			_, err := s.repository.Device.UpdateDevice(ctx, secondDevice.Id(), upSecondDeviceFn)
			if err != nil {
				return nil, fmt.Errorf("second device update error: %s", err.Error())
			}
			//sleep after device stop, to prevent short circuit
			time.Sleep(time.Second * 3)
		}
	}

	if association != nil {
		//time.Sleep(association.StateChangeDelay() * time.Second)
	}

	upFn := func(oldDevice *device.Device) (*device.Device, error) {
		return device.NewDeviceWithId(oldDevice.Id(), oldDevice.BoardId(), oldDevice.Name(), oldDevice.Pin(), newState, oldDevice.CreatedAt(), time.Now())
	}

	if association != nil && newState {
		//if its an device with association then switch it off after default time
		defer func() {
			time.Sleep(defaultTurnOfTimeForAssociations)
			upFn := func(oldDevice *device.Device) (*device.Device, error) {
				return device.NewDeviceWithId(oldDevice.Id(), oldDevice.BoardId(), oldDevice.Name(), oldDevice.Pin(), false, oldDevice.CreatedAt(), time.Now())
			}
			s.repository.Device.UpdateDevice(ctx, id, upFn)
		}()
	}

	return s.repository.Device.UpdateDevice(ctx, id, upFn)
}

func (s *service) DeleteDevice(ctx context.Context, id uuid.UUID) (err error) {
	return s.repository.Device.DeleteDevice(ctx, id)
}

func (s *service) FindDeviceById(ctx context.Context, id uuid.UUID) (device *device.Device, err error) {
	return s.repository.Device.FindDeviceById(ctx, id)
}

func (s *service) ReadDevicesList(ctx context.Context) (statuses []*device.Device, err error) {
	return s.repository.Device.ReadDevicesList(ctx)
}
