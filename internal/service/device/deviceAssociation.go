package device

import (
	"context"
	"smart-house-backend/internal/domain/device"

	"github.com/google/uuid"
)

func (s *service) CreateDeviceAssociation(ctx context.Context, device *device.DeviceAssociation) (err error) {
	return s.repository.DeviceAssociation.CreateDeviceAssociation(ctx, device)
}

func (s *service) UpdateDeviceAssociation(ctx context.Context, id uuid.UUID, updateFn func(device *device.DeviceAssociation) (*device.DeviceAssociation, error)) (device *device.DeviceAssociation, err error) {
	return s.repository.DeviceAssociation.UpdateDeviceAssociation(ctx, id, updateFn)
}

func (s *service) DeleteDeviceAssociation(ctx context.Context, id uuid.UUID) (err error) {
	return s.repository.DeviceAssociation.DeleteDeviceAssociation(ctx, id)
}

func (s *service) FindDeviceAssociationByDeviceId(ctx context.Context, deviceId uuid.UUID) (device *device.DeviceAssociation, err error) {
	return s.repository.DeviceAssociation.FindDeviceAssociationByDeviceId(ctx, deviceId)
}

func (s *service) ReadDeviceAssociationsList(ctx context.Context) (list []*device.DeviceAssociation, err error) {
	return s.repository.DeviceAssociation.ReadDeviceAssociationsList(ctx)
}
