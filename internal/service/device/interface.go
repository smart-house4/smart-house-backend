package device

import (
	"context"
	"smart-house-backend/internal/domain/device"

	"github.com/google/uuid"
)

type Device interface {
	CreateDevice(ctx context.Context, device *device.Device) (err error)
	UpdateDevice(ctx context.Context, id uuid.UUID, updateFn func(device *device.Device) (*device.Device, error)) (device *device.Device, err error)
	UpdateDeviceState(ctx context.Context, id uuid.UUID, apiKey string, newState bool) (device *device.Device, err error)
	DeleteDevice(ctx context.Context, id uuid.UUID) (err error)
	FindDeviceById(ctx context.Context, id uuid.UUID) (device *device.Device, err error)
	ReadDevicesList(ctx context.Context) (devices []*device.Device, err error)

	CreateDeviceAssociation(ctx context.Context, device *device.DeviceAssociation) (err error)
	UpdateDeviceAssociation(ctx context.Context, id uuid.UUID, updateFn func(device *device.DeviceAssociation) (*device.DeviceAssociation, error)) (device *device.DeviceAssociation, err error)
	DeleteDeviceAssociation(ctx context.Context, id uuid.UUID) (err error)
	FindDeviceAssociationByDeviceId(ctx context.Context, deviceId uuid.UUID) (device *device.DeviceAssociation, err error)
	ReadDeviceAssociationsList(ctx context.Context) (list []*device.DeviceAssociation, err error)
}
