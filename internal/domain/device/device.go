package device

import (
	"time"

	"github.com/google/uuid"
)

type Device struct {
	id         uuid.UUID
	boardId    uuid.UUID
	name       string
	pin        int
	state      bool
	createdAt  time.Time
	modifiedAt time.Time
}

func (d Device) Id() uuid.UUID {
	return d.id
}

func (d Device) BoardId() uuid.UUID {
	return d.boardId
}

func (d Device) Name() string {
	return d.name
}

func (d Device) Pin() int {
	return d.pin
}

func (d Device) State() bool {
	return d.state
}

func (d Device) CreatedAt() time.Time {
	return d.createdAt
}

func (d Device) ModifiedAt() time.Time {
	return d.modifiedAt
}

func NewDevice(
	boardId uuid.UUID,
	pin int,
	name string,
	state bool,
) (*Device, error) {
	return &Device{
		id:         uuid.New(),
		boardId:    boardId,
		name:       name,
		pin:        pin,
		state:      state,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}

func NewDeviceWithId(
	id uuid.UUID,
	boardId uuid.UUID,
	name string,
	pin int,
	state bool,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Device, error) {
	return &Device{
		id:         id,
		boardId:    boardId,
		name:       name,
		pin:        pin,
		state:      state,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}
