package device

import (
	"time"

	"github.com/google/uuid"
)

type DeviceAssociation struct {
	id               uuid.UUID
	first            uuid.UUID
	second           uuid.UUID
	stateChangeDelay time.Duration
	createdAt        time.Time
	modifiedAt       time.Time
}

func (d *DeviceAssociation) Id() uuid.UUID {
	return d.id
}

func (d *DeviceAssociation) First() uuid.UUID {
	return d.first
}

func (d *DeviceAssociation) Second() uuid.UUID {
	return d.second
}

func (d *DeviceAssociation) StateChangeDelay() time.Duration {
	return d.stateChangeDelay
}

func (d *DeviceAssociation) CreatedAt() time.Time {
	return d.createdAt
}

func (d *DeviceAssociation) ModifiedAt() time.Time {
	return d.createdAt
}

func NewDeviceAssociation(first uuid.UUID, second uuid.UUID, stateChangeDelay time.Duration) (*DeviceAssociation, error) {
	return &DeviceAssociation{
		id:               uuid.New(),
		first:            first,
		second:           second,
		stateChangeDelay: stateChangeDelay,
		createdAt:        time.Now(),
		modifiedAt:       time.Now(),
	}, nil
}

func NewDeviceAssociationWithId(id uuid.UUID, first uuid.UUID, second uuid.UUID, stateChangeDelay time.Duration, createdAt, modifiedAt time.Time) (*DeviceAssociation, error) {
	return &DeviceAssociation{
		id:               id,
		first:            first,
		second:           second,
		stateChangeDelay: stateChangeDelay,
		createdAt:        createdAt,
		modifiedAt:       modifiedAt,
	}, nil
}
