package place

import (
	"time"

	"github.com/google/uuid"
)

type Place struct {
	id         uuid.UUID
	name       string
	createdAt  time.Time
	modifiedAt time.Time
}

func (b Place) Id() uuid.UUID {
	return b.id
}

func (b Place) Name() string {
	return b.name
}

func (b Place) CreatedAt() time.Time {
	return b.createdAt
}

func (b Place) ModifiedAt() time.Time {
	return b.modifiedAt
}

func NewPlaceWithId(
	id uuid.UUID,
	name string,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Place, error) {
	return &Place{
		id:         id,
		name:       name,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func NewPlace(
	name string,
) (*Place, error) {
	return &Place{
		id:         uuid.New(),
		name:       name,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}
