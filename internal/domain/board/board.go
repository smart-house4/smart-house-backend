package board

import (
	"time"

	"github.com/google/uuid"
)

type Board struct {
	id         uuid.UUID
	placeId    uuid.UUID
	name       string
	createdAt  time.Time
	modifiedAt time.Time
}

func (b Board) Id() uuid.UUID {
	return b.id
}

func (b Board) PlaceId() uuid.UUID {
	return b.placeId
}

func (b Board) Name() string {
	return b.name
}

func (b Board) CreatedAt() time.Time {
	return b.createdAt
}

func (b Board) ModifiedAt() time.Time {
	return b.modifiedAt
}

func NewBoard(
	placeId uuid.UUID,
	name string,
) (*Board, error) {
	return &Board{
		id:         uuid.New(),
		placeId:    placeId,
		name:       name,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}

func NewBoardWithId(
	id uuid.UUID,
	placeId uuid.UUID,
	name string,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Board, error) {
	return &Board{
		id:         id,
		placeId:    placeId,
		name:       name,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}
