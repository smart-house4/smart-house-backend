package apiKey

import (
	"time"

	"github.com/google/uuid"
)

type ApiKey struct {
	id         uuid.UUID
	userId     uuid.UUID
	key        string
	createdAt  time.Time
	modifiedAt time.Time
}

func (a ApiKey) Id() uuid.UUID {
	return a.id
}

func (a ApiKey) UserId() uuid.UUID {
	return a.userId
}

func (a ApiKey) Key() string {
	return a.key
}

func (a ApiKey) CreatedAt() time.Time {
	return a.createdAt
}

func (a ApiKey) ModifiedAt() time.Time {
	return a.modifiedAt
}

func NewApiKey(
	userId uuid.UUID,
) (*ApiKey, error) {
	return &ApiKey{
		id:         uuid.New(),
		userId:     userId,
		key:        uuid.NewString(),
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}

func NewApiKeyWithId(
	id uuid.UUID,
	userId uuid.UUID,
	key string,
	createdAt time.Time,
	modifiedAt time.Time,
) (*ApiKey, error) {
	return &ApiKey{
		id:         id,
		userId:     userId,
		key:        key,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}
