package dao

import (
	"time"

	"github.com/google/uuid"
)

type Board struct {
	Id         uuid.UUID `db:"id"`
	PlaceId    uuid.UUID `db:"place_id"`
	Name       string    `db:"name"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsBoard = []string{
	"id",
	"place_id",
	"name",
	"created_at",
	"modified_at",
}
