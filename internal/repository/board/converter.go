package repository

import (
	"smart-house-backend/internal/domain/board"
	"smart-house-backend/internal/repository/board/dao"
)

func (r *Repository) toDomainBoard(daoBoard *dao.Board) (*board.Board, error) {
	return board.NewBoardWithId(daoBoard.Id, daoBoard.PlaceId, daoBoard.Name, daoBoard.CreatedAt, daoBoard.ModifiedAt)
}
