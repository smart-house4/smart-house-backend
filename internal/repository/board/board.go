package repository

import (
	"context"
	"errors"
	"fmt"
	"smart-house-backend/internal/domain/board"
	"smart-house-backend/internal/repository/board/dao"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "public.board"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"board\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateBoard(ctx context.Context, board *board.Board) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsBoard...).Values(board.Id(), board.PlaceId(), board.Name(), board.CreatedAt(), board.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = r.Pool.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateBoard(ctx context.Context, id uuid.UUID, updateFn func(board *board.Board) (*board.Board, error)) (board *board.Board, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upBoard, err := r.oneBoardTx(ctx, tx, id)
	if err != nil {
		return
	}

	boardNew, err := updateFn(upBoard)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("place_id", boardNew.PlaceId()).Set("name", boardNew.Name()).Set("modified_at", boardNew.ModifiedAt()).Where("id = ?", boardNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return
}

func (r *Repository) DeleteBoard(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) FindBoardById(ctx context.Context, id uuid.UUID) (board *board.Board, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	board, err = r.oneBoardTx(ctx, tx, id)
	return
}

func (r *Repository) ReadBoardsList(ctx context.Context) (list []*board.Board, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsBoard...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoBoard, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Board])
	if err != nil {
		return nil, err
	}

	list = []*board.Board{}

	for _, board := range daoBoard {
		boardIns, err := r.toDomainBoard(&board)
		if err != nil {
			return nil, err
		}

		list = append(list, boardIns)
	}

	return
}

func (r *Repository) oneBoardTx(ctx context.Context, tx pgx.Tx, id uuid.UUID) (response *board.Board, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsBoard...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoApiKey, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Board])
	if err != nil {
		return nil, err
	}

	return r.toDomainBoard(&daoApiKey)
}
