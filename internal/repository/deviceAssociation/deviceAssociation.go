package repository

import (
	"context"
	"errors"
	"fmt"
	"smart-house-backend/internal/domain/device"
	"smart-house-backend/internal/repository/deviceAssociation/dao"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "public.device_association"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"device_association\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateDeviceAssociation(ctx context.Context, device *device.DeviceAssociation) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsDeviceAssociation...).Values(device.Id(), device.First(), device.Second(), device.StateChangeDelay(), device.CreatedAt(), device.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = r.Pool.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateDeviceAssociation(ctx context.Context, id uuid.UUID, updateFn func(device *device.DeviceAssociation) (*device.DeviceAssociation, error)) (device *device.DeviceAssociation, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upDeviceAssociation, err := r.oneDeviceAssociationTx(ctx, tx, id)
	if err != nil {
		return
	}

	deviceNew, err := updateFn(upDeviceAssociation)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("first", deviceNew.First()).Set("second", deviceNew.Second()).Set("state_change_delay", deviceNew.StateChangeDelay()).Set("modified_at", deviceNew.ModifiedAt()).Where("id = ?", deviceNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return deviceNew, nil
}

func (r *Repository) DeleteDeviceAssociation(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) FindDeviceAssociationByDeviceId(ctx context.Context, deviceId uuid.UUID) (device *device.DeviceAssociation, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsDeviceAssociation...).From(tableName).Where("first = ? or second = ?", deviceId, deviceId)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoDeviceAssociation, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.DeviceAssociation])
	if err != nil {
		return nil, err
	}

	return r.toDomainDeviceAssociation(&daoDeviceAssociation)
}

func (r *Repository) ReadDeviceAssociationsList(ctx context.Context) (list []*device.DeviceAssociation, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsDeviceAssociation...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoDeviceAssociation, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.DeviceAssociation])
	if err != nil {
		return nil, err
	}

	list = []*device.DeviceAssociation{}

	for _, device := range daoDeviceAssociation {
		deviceIns, err := r.toDomainDeviceAssociation(&device)
		if err != nil {
			return nil, err
		}

		list = append(list, deviceIns)
	}

	return
}

func (r *Repository) oneDeviceAssociationTx(ctx context.Context, tx pgx.Tx, id uuid.UUID) (response *device.DeviceAssociation, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsDeviceAssociation...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoDeviceAssociation, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.DeviceAssociation])
	if err != nil {
		return nil, err
	}

	return r.toDomainDeviceAssociation(&daoDeviceAssociation)
}
