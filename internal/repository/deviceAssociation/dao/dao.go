package dao

import (
	"time"

	"github.com/google/uuid"
)

type DeviceAssociation struct {
	Id               uuid.UUID     `db:"id"`
	First            uuid.UUID     `db:"first"`
	Second           uuid.UUID     `db:"second"`
	StateChangeDelay time.Duration `db:"state_change_delay"`
	CreatedAt        time.Time     `db:"created_at"`
	ModifiedAt       time.Time     `db:"modified_at"`
}

var ColumnsDeviceAssociation = []string{
	"id",
	"first",
	"second",
	"state_change_delay",
	"created_at",
	"modified_at",
}
