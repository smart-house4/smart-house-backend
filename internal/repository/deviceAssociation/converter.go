package repository

import (
	"smart-house-backend/internal/domain/device"
	"smart-house-backend/internal/repository/deviceAssociation/dao"
)

func (r *Repository) toDomainDeviceAssociation(daoDevice *dao.DeviceAssociation) (*device.DeviceAssociation, error) {
	return device.NewDeviceAssociationWithId(daoDevice.Id, daoDevice.First, daoDevice.Second, daoDevice.StateChangeDelay, daoDevice.CreatedAt, daoDevice.ModifiedAt)
}
