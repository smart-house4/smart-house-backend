package repository

import (
	apiKey "smart-house-backend/internal/repository/apiKey"
	board "smart-house-backend/internal/repository/board"
	device "smart-house-backend/internal/repository/device"
	deviceAssociation "smart-house-backend/internal/repository/deviceAssociation"
	place "smart-house-backend/internal/repository/place"
	user "smart-house-backend/internal/repository/user"

	"gitlab.com/kanya384/gotools/psql"
)

type Repository struct {
	ApiKey            *apiKey.Repository
	Place             *place.Repository
	Board             *board.Repository
	Device            *device.Repository
	DeviceAssociation *deviceAssociation.Repository
	User              *user.Repository
}

func NewRepository(pg *psql.Postgres) (*Repository, error) {
	apiKey, err := apiKey.New(pg, apiKey.Options{})
	if err != nil {
		return nil, err
	}

	board, err := board.New(pg, board.Options{})
	if err != nil {
		return nil, err
	}

	place, err := place.New(pg, place.Options{})
	if err != nil {
		return nil, err
	}

	device, err := device.New(pg, device.Options{})
	if err != nil {
		return nil, err
	}

	deviceAssociation, err := deviceAssociation.New(pg, deviceAssociation.Options{})
	if err != nil {
		return nil, err
	}

	user, err := user.New(pg, user.Options{})
	if err != nil {
		return nil, err
	}

	return &Repository{
		apiKey,
		place,
		board,
		device,
		deviceAssociation,
		user,
	}, nil

}
