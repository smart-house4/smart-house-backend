package repository

import (
	"smart-house-backend/internal/domain/user"
	"smart-house-backend/internal/repository/user/dao"
)

func (r *Repository) toDomainUser(dao *dao.User) (*user.User, error) {
	return user.NewWithID(dao.Id, dao.Name, dao.Login, "", user.Role(dao.Role), dao.CreatedAt, dao.ModifiedAt) // pass is empty for security reasons
}
