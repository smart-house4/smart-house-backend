package repository

import (
	"context"
	"errors"
	"fmt"
	"smart-house-backend/internal/domain/device"
	"smart-house-backend/internal/repository/device/dao"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "public.device"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"device\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateDevice(ctx context.Context, device *device.Device) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsDevice...).Values(device.Id(), device.BoardId(), device.Name(), device.Pin(), device.State(), device.CreatedAt(), device.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = r.Pool.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateDevice(ctx context.Context, id uuid.UUID, updateFn func(device *device.Device) (*device.Device, error)) (device *device.Device, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upDevice, err := r.oneDeviceTx(ctx, tx, id)
	if err != nil {
		return
	}

	deviceNew, err := updateFn(upDevice)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("board_id", deviceNew.BoardId()).Set("name", deviceNew.Name()).Set("pin", deviceNew.Pin()).Set("state", deviceNew.State()).Set("modified_at", deviceNew.ModifiedAt()).Where("id = ?", deviceNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return
}

func (r *Repository) DeleteDevice(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) FindDeviceById(ctx context.Context, id uuid.UUID) (device *device.Device, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	device, err = r.oneDeviceTx(ctx, tx, id)
	return
}

func (r *Repository) FindDevicesByBoardId(ctx context.Context, boardId uuid.UUID) (list []*device.Device, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsDevice...).From(tableName).Where("board_id = ?", boardId)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoDevice, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Device])
	if err != nil {
		return nil, err
	}

	list = []*device.Device{}

	for _, device := range daoDevice {
		deviceIns, err := r.toDomainDevice(&device)
		if err != nil {
			return nil, err
		}

		list = append(list, deviceIns)
	}

	return
}

func (r *Repository) ReadDevicesList(ctx context.Context) (list []*device.Device, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsDevice...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoDevice, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Device])
	if err != nil {
		return nil, err
	}

	list = []*device.Device{}

	for _, device := range daoDevice {
		deviceIns, err := r.toDomainDevice(&device)
		if err != nil {
			return nil, err
		}

		list = append(list, deviceIns)
	}

	return
}

func (r *Repository) oneDeviceTx(ctx context.Context, tx pgx.Tx, id uuid.UUID) (response *device.Device, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsDevice...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoApiKey, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Device])
	if err != nil {
		return nil, err
	}

	return r.toDomainDevice(&daoApiKey)
}
