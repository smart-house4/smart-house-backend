package repository

import (
	"smart-house-backend/internal/domain/device"
	"smart-house-backend/internal/repository/device/dao"
)

func (r *Repository) toDomainDevice(daoDevice *dao.Device) (*device.Device, error) {
	return device.NewDeviceWithId(daoDevice.Id, daoDevice.BoardId, daoDevice.Name, daoDevice.Pin, daoDevice.State, daoDevice.CreatedAt, daoDevice.ModifiedAt)
}
