package dao

import (
	"time"

	"github.com/google/uuid"
)

type Device struct {
	Id         uuid.UUID `db:"id"`
	BoardId    uuid.UUID `db:"board_id"`
	Name       string    `db:"name"`
	Pin        int       `db:"pin"`
	State      bool      `db:"state"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsDevice = []string{
	"id",
	"board_id",
	"name",
	"pin",
	"state",
	"created_at",
	"modified_at",
}
