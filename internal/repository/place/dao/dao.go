package dao

import (
	"time"

	"github.com/google/uuid"
)

type Place struct {
	Id         uuid.UUID `db:"id"`
	Name       string    `db:"name"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsPlace = []string{
	"id",
	"name",
	"created_at",
	"modified_at",
}
