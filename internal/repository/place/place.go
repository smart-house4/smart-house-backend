package repository

import (
	"context"
	"errors"
	"fmt"
	"smart-house-backend/internal/domain/place"
	"smart-house-backend/internal/repository/place/dao"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "public.place"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"place\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreatePlace(ctx context.Context, place *place.Place) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsPlace...).Values(place.Id(), place.Name(), place.CreatedAt(), place.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = r.Pool.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdatePlace(ctx context.Context, id uuid.UUID, updateFn func(place *place.Place) (*place.Place, error)) (place *place.Place, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upPlace, err := r.onePlaceTx(ctx, tx, id)
	if err != nil {
		return
	}

	placeNew, err := updateFn(upPlace)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("name", placeNew.Name()).Set("modified_at", placeNew.ModifiedAt()).Where("id = ?", placeNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return
}

func (r *Repository) DeletePlace(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) FindPlaceById(ctx context.Context, id uuid.UUID) (place *place.Place, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	place, err = r.onePlaceTx(ctx, tx, id)
	return
}

func (r *Repository) ReadPlacesList(ctx context.Context) (list []*place.Place, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsPlace...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoPlace, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Place])
	if err != nil {
		return nil, err
	}

	list = []*place.Place{}

	for _, place := range daoPlace {
		placeIns, err := r.toDomainPlace(&place)
		if err != nil {
			return nil, err
		}

		list = append(list, placeIns)
	}

	return
}

func (r *Repository) onePlaceTx(ctx context.Context, tx pgx.Tx, id uuid.UUID) (response *place.Place, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsPlace...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoApiKey, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Place])
	if err != nil {
		return nil, err
	}

	return r.toDomainPlace(&daoApiKey)
}
