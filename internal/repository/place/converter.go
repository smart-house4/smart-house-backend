package repository

import (
	"smart-house-backend/internal/domain/place"
	"smart-house-backend/internal/repository/place/dao"
)

func (r *Repository) toDomainPlace(daoPlace *dao.Place) (*place.Place, error) {
	return place.NewPlaceWithId(daoPlace.Id, daoPlace.Name, daoPlace.CreatedAt, daoPlace.ModifiedAt)
}
