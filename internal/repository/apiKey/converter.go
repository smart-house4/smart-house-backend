package repository

import (
	"smart-house-backend/internal/domain/apiKey"
	"smart-house-backend/internal/repository/apiKey/dao"
)

func (r *Repository) toDomainApiKey(key *dao.ApiKey) (*apiKey.ApiKey, error) {
	return apiKey.NewApiKeyWithId(key.Id, key.UserId, key.Key, key.CreatedAt, key.ModifiedAt)
}
