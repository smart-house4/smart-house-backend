package dao

import (
	"time"

	"github.com/google/uuid"
)

type ApiKey struct {
	Id         uuid.UUID `db:"id"`
	UserId     uuid.UUID `db:"user_id"`
	Key        string    `db:"key"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsApiKey = []string{
	"id",
	"user_id",
	"key",
	"created_at",
	"modified_at",
}
