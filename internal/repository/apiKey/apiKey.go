package repository

import (
	"context"
	"errors"
	"fmt"
	"smart-house-backend/internal/domain/apiKey"
	"smart-house-backend/internal/repository/apiKey/dao"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "public.api_key"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"api_key\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateApiKey(ctx context.Context, apiKey *apiKey.ApiKey) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsApiKey...).Values(apiKey.Id(), apiKey.UserId(), apiKey.Key(), apiKey.CreatedAt(), apiKey.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = r.Pool.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateApiKey(ctx context.Context, id uuid.UUID, updateFn func(apiKey *apiKey.ApiKey) (*apiKey.ApiKey, error)) (apiKey *apiKey.ApiKey, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upUser, err := r.oneApiKeyTx(ctx, tx, id)
	if err != nil {
		return
	}

	userNew, err := updateFn(upUser)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("user_id", userNew.UserId()).Set("key", userNew.Key()).Set("modified_at", userNew.ModifiedAt()).Where("id = ?", userNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return
}

func (r *Repository) DeleteApiKey(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) FindApiKeyByKey(ctx context.Context, key string) (apiKey *apiKey.ApiKey, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsApiKey...).From(tableName).Where("key = ?", key)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoApiKey, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.ApiKey])
	if err != nil {
		return nil, err
	}

	return r.toDomainApiKey(&daoApiKey)
}

func (r *Repository) ReadApiKeyList(ctx context.Context) (list []*apiKey.ApiKey, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsApiKey...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoUser, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.ApiKey])
	if err != nil {
		return nil, err
	}

	list = []*apiKey.ApiKey{}

	for _, apiKey := range daoUser {
		apiKeyIns, err := r.toDomainApiKey(&apiKey)
		if err != nil {
			return nil, err
		}

		list = append(list, apiKeyIns)
	}

	return
}

func (r *Repository) oneApiKeyTx(ctx context.Context, tx pgx.Tx, id uuid.UUID) (response *apiKey.ApiKey, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsApiKey...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoApiKey, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.ApiKey])
	if err != nil {
		return nil, err
	}

	return r.toDomainApiKey(&daoApiKey)
}
